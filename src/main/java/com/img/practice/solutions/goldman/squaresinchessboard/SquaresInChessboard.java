package com.img.practice.solutions.goldman.squaresinchessboard;

public class SquaresInChessboard {

  private SquaresInChessboard() {
    throw new IllegalStateException("Utility class");
  }

  /**This method calculates the total number of squares for a given chessboard size.
   *
   * @param chessboardSize - size of chessboard
   * @return int - total number of squares in a chessboard.
   */
  public static int countSquares(final int chessboardSize) {

    if (chessboardSize < 1) {
      return 0;
    } else if (chessboardSize == 1) {
      return 1;
    } else {
      return (chessboardSize * (chessboardSize + 1) * ((2 * chessboardSize) + 1)) / 6;
    }
  }

}
