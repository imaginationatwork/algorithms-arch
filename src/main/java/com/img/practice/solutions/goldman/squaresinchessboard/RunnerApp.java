package com.img.practice.solutions.goldman.squaresinchessboard;

import java.util.Scanner;

/**
 * Problem Statement: Find total number of Squares in a N*N chessboard.
 *
 * <p>Input: The first line contains an integer T, depicting total number of test cases.
 * Then following T lines contains an integer N that is the side of the chessboard.
 *
 * <p>Output: Each separate line showing the maximum number of squares possible.
 *
 * <p>Constraints: 1 ≤ T ≤ 100 1 ≤ N ≤ 100
 *
 * <p>Example: Input: 2 1 2
 *
 * <p>Output: 1 5
 *
 * @see com.img.practice.solutions.goldman.squaresinchessboard.SquaresInChessboard#countSquares(int)
 */

@SuppressWarnings({"java:S106", "java:S1199"})
public class RunnerApp {

  public static void main(String[] args) {
    System.out.println("Starting application...");
    init();
  }

  private static void init() {
    System.out.println("We are now testing a new code implementation");
    System.out.println("Problem Statement: Squares in N*N Chessboard");

    boolean repeat;
    do {
      runSquaresInChessboard();
      repeat = repeat();
    } while (repeat);

    exitSystem();
  }

  private static void runSquaresInChessboard() {
    System.out.println();
    System.out.print("Please provide an integer as input denoting the size of chessboard: ");
    Scanner scanner = new Scanner(System.in);
    int input = scanner.nextInt();
    int result = SquaresInChessboard.countSquares(input);
    System.out.println(String
        .format("Total number of squares for input %d is %d",
            input, result));
  }

  private static boolean repeat() {
    System.out.print("Would you like to try another sort...? [Y/N]  ");
    Scanner scanner = new Scanner(System.in);
    return scanner.next().equalsIgnoreCase("Y");
  }


  private static void exitSystem() {
    System.out.println("User opted to stop program");
    System.out.println("Stopping system...");
  }
}
