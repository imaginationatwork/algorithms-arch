package com.img.data.algorithm.sort.utils;

import com.img.data.algorithm.sort.SortRunnerApp;
import com.img.data.algorithm.sort.functions.DriverFunctions;
import java.util.Scanner;

@SuppressWarnings({"java:S106"})
public class SysUtils {

  private SysUtils() {
    throw new IllegalStateException("Utility Class");
  }

  /**
   * performs system exit.
   *
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void exitSystem() throws InterruptedException {
    System.out.println("User opted to stop program");
    System.out.println("Stopping system...");
    Thread.sleep(2000);
    System.exit(0);
  }


  /**
   * performs prompt repetition.
   *
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void repeatPrompt() throws InterruptedException {
    System.out.print("Would you like to try another sort...? [Y/N]  ");
    Scanner scanner = new Scanner(System.in);
    if (scanner.next().equalsIgnoreCase("Y")) {
      VisUtils.visualise();
      ArrayUtils.reset();
      DriverFunctions.runSort(SortRunnerApp.getIntegers(), SortRunnerApp.getSortingAlgorithm(),
          SortRunnerApp.getSortingOrder());
      InputUtils.purgeScanner(scanner);
    }
    SysUtils.exitSystem();
  }


}
