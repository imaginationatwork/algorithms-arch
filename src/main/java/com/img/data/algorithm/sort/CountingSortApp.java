package com.img.data.algorithm.sort;

import com.img.data.algorithm.sort.utils.ArrayUtils;

public class CountingSortApp {

  /**
   * Driving the counting sort.
   * @param args - arguments passed to the amin method
   */
  public static void main(String[] args) {

    int[] integers = new int[]{2, 5, 9, 8, 2, 8, 7, 10, 4, 3};
    countingSort(integers, 1, 10);
    ArrayUtils.print(integers);
  }

  /**
   * complexity O(n) = n linear.
   * @param integers - input array.
   * @param min - lower bound of the values present in the input array.
   * @param max - upper bound of the values present in the input array.
   */
  public static void countingSort(int[] integers, int min, int max) {
    int[] countArray = new int[max - min + 1];

    for (int integer : integers) {
      countArray[integer - min]++;
    }

    int j = 0;

    for (int i = min; i <= max; i++) {

      while (countArray[i - min] > 0) {
        integers[j++] = i;
        countArray[i - min]--;
      }
    }
  }

}
