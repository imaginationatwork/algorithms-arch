package com.img.data.algorithm.sort.functions;

import com.img.data.algorithm.sort.SortRunnerApp;
import com.img.data.algorithm.sort.enums.Order;
import com.img.data.algorithm.sort.enums.SortingAlgorithm;
import com.img.data.algorithm.sort.utils.ArrayUtils;
import com.img.data.algorithm.sort.utils.SysUtils;
import com.img.data.algorithm.sort.wrappers.SortWrapper;

@SuppressWarnings({"java:S106"})
public final class DriverFunctions {

  private DriverFunctions() {
    throw new IllegalStateException("Utility class");
  }

  public static int[] initData() {
    return new int[]{20, 35, -15, 7, 55, 1, -22};
  }

  /**
   * Initializes the systems.
   *
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void init() throws InterruptedException {
    System.out.println("Initializing system ...");
    SortRunnerApp.setIntegers(DriverFunctions.initData());
    Thread.sleep(2000);
    System.out.println();
    System.out.println("Elements of array are: ");
    ArrayUtils.print(SortRunnerApp.getIntegers());
  }

  /**
   * Executes sort operation in the order opted by the user.
   *
   * @param integers         - input array
   * @param sortingAlgorithm - sorting algorithm opted by the user
   * @param sortingOrder     - sorting order opted by the user
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void runSort(final int[] integers,
      SortingAlgorithm sortingAlgorithm, Order sortingOrder) throws InterruptedException {
    SortWrapper.executeSortOperation(integers, sortingAlgorithm, sortingOrder);
    SysUtils.repeatPrompt();
  }

}
