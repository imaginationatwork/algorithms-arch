package com.img.data.algorithm.sort.utils;

import com.img.data.algorithm.sort.enums.Order;
import com.img.data.algorithm.sort.enums.SortingAlgorithm;
import java.util.Scanner;

@SuppressWarnings({"java:S106"})
public class InputUtils {

  private InputUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * gets the sorting algorithm option selected by the user.
   *
   * @return SortingAlgorithm - chosen by the user.
   */
  public static SortingAlgorithm getSelectionInput() {
    SortingAlgorithm algorithm;
    try {
      algorithm = SortingAlgorithm.values()[new Scanner(System.in).nextInt()];
    } catch (Exception e) {
      e.getSuppressed();
      algorithm = SortingAlgorithm.INVALID_INPUT;
    }
    return algorithm;
  }

  /**
   * gets the ordering option selected by the user.
   *
   * @return Order - chosen by the user
   */
  public static Order getOrderInput() {

    Order sortingOrder;
    try {
      sortingOrder = Order.values()[new Scanner(System.in).nextInt()];
    } catch (Exception e) {
      e.getSuppressed();
      sortingOrder = Order.INVALID_INPUT;
    }
    return sortingOrder;
  }

  /**
   * validates a provided input by the user.
   *
   * @param algorithm - algorithm chosen by the user.
   * @param order     - order input provided by the user
   * @return boolean - returns true if inputs are valid
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static boolean isValidInputs(SortingAlgorithm algorithm,
      Order order) throws InterruptedException {
    if (algorithm.equals(SortingAlgorithm.INVALID_INPUT) || order
        .equals(Order.INVALID_INPUT)) {
      System.out.println("You have entered invalid inputs...");
      System.out.println("Re running the app...");
      Thread.sleep(2000);
      return Boolean.FALSE;
    }
    return Boolean.TRUE;
  }

  /**
   * purges a given scanner object.
   *
   * @param scanner - the scanner needs to be purged
   */
  public static void purgeScanner(Scanner scanner) {
    while (scanner.hasNext()) {
      scanner.next();
    }
  }

}
