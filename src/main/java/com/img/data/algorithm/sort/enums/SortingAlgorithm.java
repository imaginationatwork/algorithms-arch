package com.img.data.algorithm.sort.enums;

import com.img.data.algorithm.sort.Sort;
import com.img.data.algorithm.sort.utils.SysUtils;
import java.util.EnumSet;
import java.util.Set;

@SuppressWarnings("java:S106")
public enum SortingAlgorithm {
  BUBBLE_SORT {
    @Override
    public void execute(int[] integers) {
      System.out.println("Executing bubble sort...");
      Sort.bubbleSort(integers);
    }
  },
  INSERTION_SORT {
    @Override
    public void execute(int[] integers) {
      System.out.println("Executing insertion sort...");
      Sort.insertionSort(integers);
    }
  },
  SELECTION_SORT {
    @Override
    public void execute(int[] integers) {
      System.out.println("Executing selection sort...");
      Sort.selectionSort(integers);
    }
  },
  SHELL_SORT {
    @Override
    public void execute(int[] integers) {
      System.out.println("Executing shell sort...");
      Sort.shellSort(integers);
    }
  },
  MERGE_SORT {
    @Override
    public void execute(int[] integers) {
      System.out.println("Executing merge sort...");
      Sort.mergeSort(integers, 0, integers.length);
    }
  },
  QUICK_SORT {
    @Override
    public void execute(int[] integers) {
      System.out.println("Executing quick sort...");
      Sort.quickSort(integers, 0, integers.length);
    }
  },
  INVALID_INPUT {
    @Override
    public void execute(int[] integers) {
      repeatHandler();
    }
  };

  public abstract void execute(int[] integers);

  private static final EnumSet<SortingAlgorithm> ALL_VALID_INPUTS =
      EnumSet.complementOf(EnumSet.of(SortingAlgorithm.INVALID_INPUT));


  public static Set<SortingAlgorithm> getAllValidInputs() {
    return ALL_VALID_INPUTS;
  }

  void repeatHandler() {
    try {
      SysUtils.repeatPrompt();
    } catch (InterruptedException e) {
      e.getSuppressed();
      Thread.currentThread().interrupt();
      System.out.println("Could not reinitialise system... ");
      System.out.println("System exiting...");
      System.exit(1);
    }
  }
}
