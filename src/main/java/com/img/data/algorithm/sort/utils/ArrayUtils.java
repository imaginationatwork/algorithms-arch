package com.img.data.algorithm.sort.utils;

import com.img.data.algorithm.sort.SortRunnerApp;
import com.img.data.algorithm.sort.functions.DriverFunctions;
import java.util.Arrays;

@SuppressWarnings("java:S106")
public final class ArrayUtils {

  private ArrayUtils() {
    throw new IllegalStateException("Utility class");
  }

  public static void print(final int[] integers) {
    Arrays.stream(integers).forEach(i -> System.out.print(i + " "));
    System.out.println();
  }

  /**
   * This function swaps two elements of an integer array.
   *
   * @param array - input array which's elements needs to be swapped
   * @param i     -  one element
   * @param j     - other element
   */
  public static void swapElements(final int[] array, final int i,
      final int j) {
    if (i == j) {
      return;
    }
    int temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

  /**
   * This is the implementation of reversing an array of any size.
   *
   * @param ints - integer array provided as input
   */
  public static void reverse(int[] ints) {
    int n = ints.length;
    for (int i = 0; i < n / 2; i++) {
      int t = ints[i];
      ints[i] = ints[n - i - 1];
      ints[n - i - 1] = t;
    }
  }

  /**
   * Resets the array of this to original values.
   */
  public static void reset() {
    SortRunnerApp.setIntegers(DriverFunctions.initData());
  }
}
