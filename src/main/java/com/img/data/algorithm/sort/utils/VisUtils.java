package com.img.data.algorithm.sort.utils;

import com.img.data.algorithm.sort.SortRunnerApp;
import com.img.data.algorithm.sort.effects.Effects;

@SuppressWarnings({"java:S106"})
public class VisUtils {

  private VisUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * shows available sort algorithms and ordering options to the user.
   *
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void visualise() throws InterruptedException {

    Thread.sleep(2000);
    System.out.println();
    Effects.showAvailableOptions();
    SortRunnerApp.setSortingAlgorithm(InputUtils.getSelectionInput());
    Effects.showAvailableOrderingOptions();
    SortRunnerApp.setSortingOrder(InputUtils.getOrderInput());
    if (!InputUtils
        .isValidInputs(SortRunnerApp.getSortingAlgorithm(), SortRunnerApp.getSortingOrder())) {
      SysUtils.repeatPrompt();
    }
    Effects.printPostSelectionMessage(SortRunnerApp.getSortingAlgorithm(),
        SortRunnerApp.getSortingOrder());
  }

}
