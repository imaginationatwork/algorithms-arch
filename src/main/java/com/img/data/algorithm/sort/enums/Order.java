package com.img.data.algorithm.sort.enums;

import java.util.EnumSet;
import java.util.Set;

public enum Order {
  ASC, DESC, INVALID_INPUT;

  private static final EnumSet<Order> GET_ALL_VALID_ORDER_INPUTS =
      EnumSet.complementOf(EnumSet.of(INVALID_INPUT));

  public static Set<Order> getAllOrderInputs() {
    return GET_ALL_VALID_ORDER_INPUTS;
  }
}


