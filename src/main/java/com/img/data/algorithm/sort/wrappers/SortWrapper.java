package com.img.data.algorithm.sort.wrappers;


import com.img.data.algorithm.sort.effects.Effects;
import com.img.data.algorithm.sort.enums.Order;
import com.img.data.algorithm.sort.enums.SortingAlgorithm;
import com.img.data.algorithm.sort.utils.ArrayUtils;

@SuppressWarnings({"java:S106"})
public final class SortWrapper {

  private SortWrapper() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Based on the option selected by the user this method executes the sorting algorithm.
   *
   * @param integers  - input array
   * @param algorithm - algorithm chosen by the user
   * @param order     - the order of sorting selected by the user
   * @see com.img.data.algorithm.sort.Sort#bubbleSort(int[])
   * @see com.img.data.algorithm.sort.Sort#selectionSort(int[])
   * @see com.img.data.algorithm.sort.Sort#insertionSort(int[])
   * @see com.img.data.algorithm.sort.Sort#shellSort(int[])
   * @see com.img.data.algorithm.sort.Sort#mergeSort(int[], int, int)
   * @see com.img.data.algorithm.sort.Sort#quickSort(int[], int, int)
   */
  public static void executeSortOperation(final int[] integers, final SortingAlgorithm algorithm,
      final Order order) throws InterruptedException {

    Effects.preSortingMessage(integers);
    algorithm.execute(integers);
    if (order.equals(Order.DESC)) {
      ArrayUtils.reverse(integers);
    }
    Effects.postSortingMessage(integers, algorithm, order);
  }
}
