package com.img.data.algorithm.sort.effects;


import com.img.data.algorithm.sort.enums.Order;
import com.img.data.algorithm.sort.enums.SortingAlgorithm;
import com.img.data.algorithm.sort.utils.ArrayUtils;

@SuppressWarnings("java:S106")
public final class Effects {

  private static final long DEFAULT_DELAY_BETWEEN_MESSAGES = 1000L;

  private Effects() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * prints post sorting message.
   *
   * @param integers             - integer array
   * @param algorithm - selected algorithm of choice
   * @param order            - selected order of printing
   */
  public static void postSortingMessage(final int[] integers, final SortingAlgorithm algorithm,
      final Order order) {
    System.out.println(
        "sort algorithm: ".concat(algorithm.name()).concat("\n").concat("Order: ")
            .concat(order.name()));
    System.out.println("Array after sorting: ");
    ArrayUtils.print(integers);
    System.out.println();
  }

  /**
   * prints pre sorting message.
   *
   * @param integers - integer array
   * @throws InterruptedException - thrown if thread is interrupted
   */
  public static void preSortingMessage(int[] integers) throws InterruptedException {
    System.out.println();
    System.out.println("Array before sorting: ");
    ArrayUtils.print(integers);
    System.out.println("Sorting ...");
    Thread.sleep(DEFAULT_DELAY_BETWEEN_MESSAGES / 2);
  }

  /**
   * prints post selection message.
   *
   * @param algorithm - algorithm chosen by the user
   * @param order     - order chosen by the user
   */
  public static void printPostSelectionMessage(
      final SortingAlgorithm algorithm,
      final Order order) {
    System.out.println();
    System.out.println(
        "Your selected input is : ".concat(algorithm.name())
            .concat(" and ordering option is ").concat(order.name()));
  }

  /**
   * Shows available algorithm options.
   *
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void showAvailableOptions() throws InterruptedException {
    System.out.println();
    System.out.println("Please enter your input, your options are");
    Thread.sleep(DEFAULT_DELAY_BETWEEN_MESSAGES);
    SortingAlgorithm.getAllValidInputs().forEach(
        option -> System.out.println(option.ordinal() + ". " + option.name()));
    System.out.println();
    Thread.sleep(DEFAULT_DELAY_BETWEEN_MESSAGES);
    System.out.print("Please enter your input here: ");
  }

  /**
   * Shows available ordering options.
   *
   * @throws InterruptedException - is thrown when a thread is interrupted
   */
  public static void showAvailableOrderingOptions()
      throws InterruptedException {
    System.out.println();
    System.out.println(
        "These are your available ordering options, Kindly select one: ");
    System.out.println();
    Order.getAllOrderInputs().forEach(
        order -> System.out.println(order.ordinal() + (". ") + order.name()));
    System.out.println();
    Thread.sleep(DEFAULT_DELAY_BETWEEN_MESSAGES);
    System.out.print("Please enter your input here: ");
  }

}
