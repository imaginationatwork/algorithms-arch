package com.img.data.algorithm.sort;

import com.img.data.algorithm.sort.enums.Order;
import com.img.data.algorithm.sort.enums.SortingAlgorithm;
import com.img.data.algorithm.sort.functions.DriverFunctions;
import com.img.data.algorithm.sort.utils.VisUtils;
import lombok.SneakyThrows;

/**
 * Demo program for Sorting algorithm.
 *
 * @author sujay
 * @version 1.0
 */
@SuppressWarnings({"java:S106"})
public class SortRunnerApp {

  private static int[] integers;
  private static Order sortingOrder;
  private static SortingAlgorithm sortingAlgorithm;

  /**
   * This is the main method that drives this program. This program shows different types of sorting
   * algorithm at play
   *
   * @param args - arguments passed to the main method
   */
  @SneakyThrows
  public static void main(String[] args) {

    DriverFunctions.init();
    VisUtils.visualise();
    DriverFunctions.runSort(integers, sortingAlgorithm, sortingOrder);
  }

  //setters
  public static void setIntegers(int[] integers) {
    SortRunnerApp.integers = integers;
  }

  public static void setSortingOrder(Order sortingOrder) {
    SortRunnerApp.sortingOrder = sortingOrder;
  }

  public static void setSortingAlgorithm(
      SortingAlgorithm sortingAlgorithm) {
    SortRunnerApp.sortingAlgorithm = sortingAlgorithm;
  }

  //getters
  public static int[] getIntegers() {
    return integers;
  }

  public static Order getSortingOrder() {
    return sortingOrder;
  }

  public static SortingAlgorithm getSortingAlgorithm() {
    return sortingAlgorithm;
  }


}
