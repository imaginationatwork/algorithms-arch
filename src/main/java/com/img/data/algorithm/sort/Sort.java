package com.img.data.algorithm.sort;

import com.img.data.algorithm.sort.utils.ArrayUtils;

public final class Sort {

  /**
   * let no one instantiate this class.
   */
  private Sort() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * swap elements of i with i+1 index if and only if i> i+1; complexity O(n). is quadratic (n
   * squired) in place algorithm - not creating a new array stable sort.
   *
   * @param integers - integer array provided as input
   */
  public static void bubbleSort(int[] integers) {
    for (int unSortedPartitionIndex = (integers.length - 1);
        unSortedPartitionIndex > 0; unSortedPartitionIndex--) {
      for (int i = 0; i < unSortedPartitionIndex; i++) {
        if (integers[i] > integers[i + 1]) {
          ArrayUtils.swapElements(integers, i, i + 1);
        }
      }
    }
  }

  /**
   * push the largest value in the unsorted partition to the last index of the unsorted partition.
   * complexity O(n) is quadratic (n squired) in place algorithm - not creating a new array unstable
   * sort.
   *
   * @param integers - input integer array.
   */
  public static void selectionSort(final int[] integers) {
    for (int unsortedPartitionIndex = (integers.length - 1);
        unsortedPartitionIndex > 0; unsortedPartitionIndex--) {
      int largest = 0;
      for (int i = 0; i < unsortedPartitionIndex; i++) {
        if (integers[i] > integers[largest]) {
          largest = i;
        }
      }
      if (integers[largest] > integers[unsortedPartitionIndex]) {
        ArrayUtils.swapElements(integers, largest, unsortedPartitionIndex);
      }
    }
  }

  /**
   * grow the sort partition by moving the smallest element to the front of the array. complexity
   * O(n). is quadratic (n squired) in place algorithm - not creating a new array stable sort.
   *
   * @param integers - integer array provided as input
   */
  public static void insertionSort(int[] integers) {
    for (int firstUnSortedPartitionIndex = 1;
        firstUnSortedPartitionIndex < integers.length;
        firstUnSortedPartitionIndex++) {
      int newElementToBeInserted = integers[firstUnSortedPartitionIndex];
      int i;
      for (i = firstUnSortedPartitionIndex;
          i > 0 && integers[i - 1] > newElementToBeInserted; i--) {
        integers[i] = integers[i - 1];
      }
      integers[i] = newElementToBeInserted;
    }

  }

  /**
   * shell short is a variation of insertion sort. it uses gap value instead of comparing the
   * immediate neighbours in place algorithm stable sort complexity O(n) = n squired (quadratic)
   *
   * @param integers - input array
   */
  public static void shellSort(int[] integers) {

    for (int gap = integers.length / 2; gap > 0; gap /= 2) {

      for (int i = gap; i < integers.length; i++) {

        int newElement = integers[i];

        int j = i;

        while (j >= gap && integers[j - gap] > newElement) {

          integers[j] = integers[j - gap];
          j -= gap;

        }

        integers[j] = newElement;
      }
    }

  }

  /**
   * this algorithm follows divide and conquer process. it's not an in place and the algorithm. it
   * is a stable algorithm complexity O(n) = nlog(n)
   *
   * @param integers - input integers
   */
  public static void mergeSort(int[] integers, int startIndex, int endIndex) {

    if (endIndex - startIndex < 2) {
      return;
    }
    int mid = (startIndex + endIndex) / 2;

    mergeSort(integers, startIndex, mid);
    mergeSort(integers, mid, endIndex);

    merge(integers, startIndex, mid, endIndex);

  }

  private static void merge(int[] integers, int start, int mid, int end) {

    if (integers[mid - 1] <= integers[mid]) {
      return;
    }
    int i = start;
    int j = mid;
    int tempIndex = 0;

    int[] temp = new int[end - start];

    while (i < mid && j < end) {
      temp[tempIndex++] = integers[i] <= integers[j] ? integers[i++] : integers[j++];
    }

    System.arraycopy(integers, i, integers, start + tempIndex, mid - i);
    System.arraycopy(temp, 0, integers, start, tempIndex);
  }

  /**
   * this algorithm follows divide and conquer process. it's an in place and the algorithm. it is a
   * unstable algorithm complexity O(n) = nlog(n)
   *
   * @param integers - input integers
   */
  public static void quickSort(int[] integers, int start, int end) {

    if (end - start < 2) {
      return;
    }

    int pivotIndex = partition(integers, start, end);
    quickSort(integers, start, pivotIndex);
    quickSort(integers, pivotIndex + 1, end);

  }

  @SuppressWarnings("all")
  private static int partition(int[] integers, int start, int end) {
    int pivot = integers[start];
    int i = start;
    int j = end;
    while (i < j) {

      //looking from right to left and shifting all the element that is less than pivot
      while (i < j && integers[--j] >= pivot) {
        //INFO: empty loop boody
      }
      if (i < j) {
        integers[i] = integers[j];
      }

      //looking from left to right and shifting the all the element that is greater than pivot
      while (i < j && integers[++i] <= pivot) {
        //INFO: empty loop body
      }
      if (i < j) {
        integers[j] = integers[i];
      }
    }

    integers[j] = pivot;
    return j;
  }
}
